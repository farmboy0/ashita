About
=====

My personal Ashita scripts and Ashitacast files.

The LICENSE doesnt cover the ashitacast.xsd which has been taken from
https://forums.ashitaxi.com/viewtopic.php?f=22&t=201
Nor does it apply to the Ashitacast/docs directory which has been taken from
Ashita and Windower.
