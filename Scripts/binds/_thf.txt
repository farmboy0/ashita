/text Apps      text 'Apps  1|SA     2|TA     3|       4|       5|       6|       7|Steal  8|Mug    9|Hide   0|Flee   '
/text AppsShift text 'Apps+ 1|       2|       3|       4|       5|       6|       7|       8|       9|       0|       '

/bind #1 /input /ja "Sneak Attack" <me>
/bind #2 /input /ja "Trick Attack" <me>
/bind #3 /echo Apps-3: not bound.
/bind #4 /echo Apps-4: not bound.
/bind #5 /echo Apps-5: not bound.
/bind #6 /echo Apps-6: not bound.
/bind #7 /input /ja "Steal" <t>
/bind #8 /input /ja "Mug" <t>
/bind #9 /input /ja "Hide" <me>
/bind #0 /input /ja "Flee" <me>

/bind #+1 /echo Apps-Shift-1: not bound.
/bind #+2 /echo Apps-Shift-2: not bound.
/bind #+3 /echo Apps-Shift-3: not bound.
/bind #+4 /echo Apps-Shift-4: not bound.
/bind #+5 /echo Apps-Shift-5: not bound.
/bind #+6 /echo Apps-Shift-6: not bound.
/bind #+7 /echo Apps-Shift-7: not bound.
/bind #+8 /echo Apps-Shift-8: not bound.
/bind #+9 /echo Apps-Shift-9: not bound.
/bind #+0 /echo Apps-Shift-0: not bound.
