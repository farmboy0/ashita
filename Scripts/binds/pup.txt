/text CtrlShift text 'Ctrl+ 1|Deacti 2|       3|       4|       5|1Hr    6|96Hr   7|       8|       9|DeusEx 0|Activat'
/text Ctrl      text 'Ctrl  1|Fire   2|Ice    3|Wind   4|Earth  5|Thunder6|Water  7|Light  8|Dark   9|       0|Cooldwn'
/text Alt       text 'Alt   1|Deploy 2|Retriev3|TPXchg 4|EnmXchg5|       6|       7|       8|HPXchg 9|Maint  0|Repair '
/text AltShift  text 'Alt+  1|SSpiral2|TorKick3|Asuran 4|Dragon 5|       6|       7|       8|       9|       0|       '

/bind ^1 /input /ja "Fire Maneuver" <me>
/bind ^2 /input /ja "Ice Maneuver" <me>
/bind ^3 /input /ja "Wind Maneuver" <me>
/bind ^4 /input /ja "Earth Maneuver" <me>
/bind ^5 /input /ja "Thunder Maneuver" <me>
/bind ^6 /input /ja "Water Maneuver" <me>
/bind ^7 /input /ja "Light Maneuver" <me>
/bind ^8 /input /ja "Dark Maneuver" <me>
/bind ^9 /echo Ctrl-9: not bound.
/bind ^0 /input /ja "Cooldown" <me>

/bind ^+1 /input /pet "Deactivate" <me>
/bind ^+2 /echo Ctrl-Shift-2: not bound.
/bind ^+3 /echo Ctrl-Shift-3: not bound.
/bind ^+4 /echo Ctrl-Shift-4: not bound.
/bind ^+5 /input /ja "Overdrive" <me>
/bind ^+6 /input /ja "Heady Artifice" <me>
/bind ^+7 /echo Ctrl-Shift-7: not bound.
/bind ^+8 /echo Ctrl-Shift-8: not bound.
/bind ^+9 /input /ja "Deus Ex Automata" <me>
/bind ^+0 /input /ja "Activate" <me>

/bind !1 /input /pet "Deploy" <t>
/bind !2 /input /pet "Retrieve" <me>
/bind !3 /input /ja "Tactical Switch" <me>
/bind !4 /input /ja "Ventriloquy" <t>
/bind !5 /echo Alt-5: not bound.
/bind !6 /echo Alt-6: not bound.
/bind !7 /echo Alt-7: not bound.
/bind !8 /input /ja "Role Reversal" <me>
/bind !9 /input /ja "Maintenance" <me>
/bind !0 /input /ja "Repair" <me>

/bind !+1 /input /ws "Shijin Spiral" <t>
/bind !+2 /input /ws "Tornado Kick" <t>
/bind !+3 /input /ws "Asuran Fists" <t>
/bind !+4 /input /ws "Dragon Kick" <t>
/bind !+5 /echo Alt-Shift-5: not bound.
/bind !+6 /echo Alt-Shift-6: not bound.
/bind !+7 /echo Alt-Shift-7: not bound.
/bind !+8 /echo Alt-Shift-8: not bound.
/bind !+9 /echo Alt-Shift-9: not bound.
/bind !+0 /echo Alt-Shift-0: not bound.
