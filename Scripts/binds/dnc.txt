/text CtrlShift text 'Ctrl+ 1|Haste  2|Drain3 3|Aspir2 4|Drain2 5|Aspir  6|Drain  7|       8|       9|       0|       '
/text Ctrl      text 'Ctrl  1|CWaltz42|DWaltz23|HWaltz 4|CWaltz55|CWaltz36|DWaltz 7|CWaltz28|Cwaltz 9|ChocJig0|SpecJig'
/text Alt       text 'Alt   1|Eva-   2|Def-   3|MRes-  4|CritEv-5|Presto 6|NFR    7|       8|Contrad9|FanDanc0|Saber  '
/text AltShift  text 'Alt+  1|Critx5 2|TA     3|DA     4|WS+    5|FM2TP  6|       7|SkChain8|Gravity9|Provoke0|Stun   '

/bind ^1 /input /ja "Curing Waltz IV" <stpc>
/bind ^2 /input /ja "Divine Waltz II" <stpc>
/bind ^3 /input /ja "Healing Waltz" <stpc>
/bind ^4 /input /ja "Curing Waltz V" <stpc>
/bind ^5 /input /ja "Curing Waltz III" <stpc>
/bind ^6 /input /ja "Divine Waltz I" <stpc>
/bind ^7 /input /ja "Curing Waltz II" <stpc>
/bind ^8 /input /ja "Curing Waltz I" <stpc>
/bind ^9 /input /ja "Chocobo Jig II" <me>
/bind ^0 /input /ja "Spectral Jig" <me>

/bind ^+1 /input /ja "Haste Samba" <me>
/bind ^+2 /input /ja "Drain Samba III" <me>
/bind ^+3 /input /ja "Aspir Samba II" <me>
/bind ^+4 /input /ja "Drain Samba II" <me>
/bind ^+5 /input /ja "Aspir Samba I" <me>
/bind ^+6 /input /ja "Drain Samba I" <me>
/bind ^+7 /echo Ctrl-Shift-7: not bound.
/bind ^+8 /echo Ctrl-Shift-8: not bound.
/bind ^+9 /echo Ctrl-Shift-9: not bound.
/bind ^+0 /echo Ctrl-Shift-0: not bound.

/bind !1 /input /ja "Quickstep" <t>
/bind !2 /input /ja "Box Step" <t>
/bind !3 /input /ja "Stutter Step" <t>
/bind !4 /input /ja "Feather Step" <t>
/bind !5 /input /ja "Presto" <me>
/bind !6 /input /ja "No Foot Rise" <me>
/bind !7 /echo Alt-7: not bound.
/bind !8 /input /ja "Contradance" <me>
/bind !9 /input /ja "Fan Dance" <me>
/bind !0 /input /ja "Saber Dance" <me>

/bind !+1 /input /ja "Climactic Flourish" <me>
/bind !+2 /input /ja "Ternary Flourish" <me>
/bind !+3 /input /ja "Striking Flourish" <me>
/bind !+4 /input /ja "Building Flourish" <me>
/bind !+5 /input /ja "Reverse Flourish" <me>
/bind !+6 /echo Alt-Shift-6: not bound.
/bind !+7 /input /ja "Wild Flourish" <t>
/bind !+8 /input /ja "Desperate Flourish" <t>
/bind !+9 /input /ja "Animated Flourish" <stnpc>
/bind !+0 /input /ja "Violent Flourish" <t>
