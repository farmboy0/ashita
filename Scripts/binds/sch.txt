/exec binds/sch.noarts.txt

/text CtrlShift text 'Ctrl+ 1|Thstrm 2|Hlstrm 3|Fistrm 4|Wnstrm 5|Rnstrm 6|Sastrm 7|Arstrm 8|Vdstrm 9|Sneak  0|Invis  '
/text Ctrl      text 'Ctrl  1|Cure3  2|Cure4  3|Regen5 4|Regain 5|Enmty- 6|Enmty+ 7|SSkin  8|Blink  9|Prot5  0|Shell5 '
/text AltShift  text 'Alt+  1|Thund3 2|Blizz3 3|Fire3  4|Aero3  5|Water3 6|Stone3 7|Klmfrm 8|Drain  9|Aspir  0|Aspir2 '
/text Win       text 'Win   1|Thhelx 2|Icehlx 3|Firhlx 4|Wndhlx 5|Wtrhlx 6|Ethhlx 7|Lihhlx 8|Drkhlx 9|IceSpk 0|ShckSpk'

/bind ^1 /input /ma "Cure III" <stpc>
/bind ^2 /input /ma "Cure IV" <stpc>
/bind ^3 /input /ma "Regen V" <stpc>
/bind ^4 /input /ma "Adloquium" <stpc>
/bind ^5 /input /ma "Animus Minuo" <stpc>
/bind ^6 /input /ma "Animus Augeo" <stpc>
/bind ^7 /input /ma "Stoneskin" <me>
/bind ^8 /input /ma "Blink" <me>
/bind ^9 /input /ma "Protect V" <stpc>
/bind ^0 /input /ma "Shell V" <stpc>

/bind ^+1 /input /ma "Thunderstorm" <stpc>
/bind ^+2 /input /ma "Hailstorm" <stpc>
/bind ^+3 /input /ma "Firestorm" <stpc>
/bind ^+4 /input /ma "Windstorm" <stpc>
/bind ^+5 /input /ma "Rainstorm" <stpc>
/bind ^+6 /input /ma "Sandstorm" <stpc>
/bind ^+7 /input /ma "Aurorastorm" <stpc>
/bind ^+8 /input /ma "Voidstorm" <stpc>
/bind ^+9 /input /ma "Sneak" <me>
/bind ^+0 /input /ma "Invisible" <me>

/bind !+1 /input /ma "Thunder III" <t>
/bind !+2 /input /ma "Blizzard III" <t>
/bind !+3 /input /ma "Fire III" <t>
/bind !+4 /input /ma "Aero III" <t>
/bind !+5 /input /ma "Water III" <t>
/bind !+6 /input /ma "Stone III" <t>
/bind !+7 /input /ma "Klimaform" <me>
/bind !+8 /input /ma "Drain" <t>
/bind !+9 /input /ma "Aspir" <t>
/bind !+0 /input /ma "Aspir II" <t>

/bind @1 /input /ma "Ionohelix" <t>
/bind @2 /input /ma "Cryohelix" <t>
/bind @3 /input /ma "Pyrohelix" <t>
/bind @4 /input /ma "Anemohelix" <t>
/bind @5 /input /ma "Hydrohelix" <t>
/bind @6 /input /ma "Geohelix" <t>
/bind @7 /input /ma "Noctohelix" <t>
/bind @8 /input /ma "Luminohelix" <t>
/bind @9 /input /ma "Ice Spikes" <me>
/bind @0 /input /ma "Shock Spikes" <me>
