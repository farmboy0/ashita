/text Win       text 'Win   1|       2|       3|       4|       5|       6|       7|       8|       9|       0|       '
/text WinSift   text 'Win+  1|       2|       3|       4|       5|       6|       7|       8|       9|       0|       '

/bind @1 /echo Win-1: not bound.
/bind @2 /echo Win-2: not bound.
/bind @3 /echo Win-3: not bound.
/bind @4 /echo Win-4: not bound.
/bind @5 /echo Win-5: not bound.
/bind @6 /echo Win-6: not bound.
/bind @7 /echo Win-7: not bound.
/bind @8 /echo Win-8: not bound.
/bind @9 /echo Win-9: not bound.
/bind @0 /echo Win-0: not bound.

/bind @+1 /echo Win-Shift-1: not bound.
/bind @+2 /echo Win-Shift-2: not bound.
/bind @+3 /echo Win-Shift-3: not bound.
/bind @+4 /echo Win-Shift-4: not bound.
/bind @+5 /echo Win-Shift-5: not bound.
/bind @+6 /echo Win-Shift-6: not bound.
/bind @+7 /echo Win-Shift-7: not bound.
/bind @+8 /echo Win-Shift-8: not bound.
/bind @+9 /echo Win-Shift-9: not bound.
/bind @+0 /echo Win-Shift-0: not bound.
