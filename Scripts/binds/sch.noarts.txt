/text Alt       text 'Alt   1|       2|       3|       4|       5|       6|LA     7|DA     8|ModVer 9|Enligh 0|Sublim '
/text WinSift   text 'Win+  1|       2|       3|       4|       5|       6|       7|       8|       9|       0|       '

/bind !1 /echo Alt-1: not bound.
/bind !2 /echo Alt-2: not bound.
/bind !3 /echo Alt-3: not bound.
/bind !4 /echo Alt-4: not bound.
/bind !5 /echo Alt-5: not bound.
/bind !6 /exec binds/sch.lightarts.txt
/bind !7 /exec binds/sch.darkarts.txt
/bind !8 /input /ja "Modus Veritas" <t>
/bind !9 /input /ja "Enlightenment" <me>
/bind !0 /input /ja "Sublimation" <me>

/bind @+1 /echo Win-Shift-1: not bound.
/bind @+2 /echo Win-Shift-2: not bound.
/bind @+3 /echo Win-Shift-3: not bound.
/bind @+4 /echo Win-Shift-4: not bound.
/bind @+5 /echo Win-Shift-5: not bound.
/bind @+6 /echo Win-Shift-6: not bound.
/bind @+7 /echo Win-Shift-7: not bound.
/bind @+8 /echo Win-Shift-8: not bound.
/bind @+9 /echo Win-Shift-9: not bound.
/bind @+0 /echo Win-Shift-0: not bound.
