/text CtrlShift text 'Ctrl+ 1|Eva-   2|Def-   3|MEva-  4|Mdef-  5|Acc-   6|Macc-  7|Mattk- 8|Haste  9|Regen  0|Refresh'
/text Ctrl      text 'Ctrl  1|Acc+   2|Att+   3|Macc+  4|Mattk+ 5|Eva+   6|MEva+  7|Mdef+  8|Haste  9|Regen  0|Refresh'
/text Alt       text 'Alt   1|Entrust2|ThFocus3|Lasting4|LifeCy 5|Demat  6|+25%   7|+50%   8|LuoDmg 9|LuoMP  0|LuoAway'
/text AltShift  text 'Alt+  1|Thund4 2|Blizz4 3|Fire4  4|Aero4  5|Water4 6|Stone4 7|Sleep2 8|Drain  9|Aspir  0|Aspir2 '

/bind !1 /input /ja "Entrust" <me>
/bind !2 /input /ja "Theurgic Focus" <me>
/bind !3 /input /ja "Lasting Emanation" <me>
/bind !4 /input /ja "Life Cycle" <me>
/bind !5 /input /ja "Dematerialize" <me>
/bind !6 /input /ja "Ecliptic Attrition" <me>
/bind !7 /input /ja "Blaze of Glory" <me>
/bind !8 /input /ja "Concentric Pulse" <t>
/bind !9 /input /ja "Radial Arcana" <me>
/bind !0 /input /ja "Full Circle" <me>

/bind ^1 /input /ma "Indi-Precision" <stpc>
/bind ^2 /input /ma "Indi-Fury" <stpc>
/bind ^3 /input /ma "Indi-Focus" <stpc>
/bind ^4 /input /ma "Indi-Acumen" <stpc>
/bind ^5 /input /ma "Indi-Voidance" <stpc>
/bind ^6 /input /ma "Indi-Attunement" <stpc>
/bind ^7 /input /ma "Indi-Fend" <stpc>
/bind ^8 /input /ma "Indi-Haste" <stpc>
/bind ^9 /input /ma "Indi-Regen" <stpc>
/bind ^0 /input /ma "Indi-Refresh" <stpc>

/bind ^+1 /input /ma "Geo-Torpor" <t>
/bind ^+2 /input /ma "Geo-Frailty" <t>
/bind ^+3 /input /ma "Geo-Languor" <t>
/bind ^+4 /input /ma "Geo-Malaise" <t>
/bind ^+5 /input /ma "Geo-Slip" <t>
/bind ^+6 /input /ma "Geo-Vex" <t>
/bind ^+7 /input /ma "Geo-Fade" <t>
/bind ^+8 /input /ma "Geo-Haste" <me>
/bind ^+9 /input /ma "Geo-Regen" <me>
/bind ^+0 /input /ma "Geo-Refresh" <me>

/bind !+1 /input /ma "Thunder IV" <t>
/bind !+2 /input /ma "Blizzard IV" <t>
/bind !+3 /input /ma "Fire IV" <t>
/bind !+4 /input /ma "Aero IV" <t>
/bind !+5 /input /ma "Water IV" <t>
/bind !+6 /input /ma "Stone IV" <t>
/bind !+7 /input /ma "Sleep II" <stnpc>
/bind !+8 /input /ma "Drain" <t>
/bind !+9 /input /ma "Aspir" <t>
/bind !+0 /input /ma "Aspir II" <t>
