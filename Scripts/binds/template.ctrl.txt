/text CtrlShift text 'Ctrl+ 1|       2|       3|       4|       5|       6|       7|       8|       9|       0|       '
/text Ctrl      text 'Ctrl  1|       2|       3|       4|       5|       6|       7|       8|       9|       0|       '

/bind ^1 /echo Ctrl-1: not bound.
/bind ^2 /echo Ctrl-2: not bound.
/bind ^3 /echo Ctrl-3: not bound.
/bind ^4 /echo Ctrl-4: not bound.
/bind ^5 /echo Ctrl-5: not bound.
/bind ^6 /echo Ctrl-6: not bound.
/bind ^7 /echo Ctrl-7: not bound.
/bind ^8 /echo Ctrl-8: not bound.
/bind ^9 /echo Ctrl-9: not bound.
/bind ^0 /echo Ctrl-0: not bound.

/bind ^+1 /echo Ctrl-Shift-1: not bound.
/bind ^+2 /echo Ctrl-Shift-2: not bound.
/bind ^+3 /echo Ctrl-Shift-3: not bound.
/bind ^+4 /echo Ctrl-Shift-4: not bound.
/bind ^+5 /echo Ctrl-Shift-5: not bound.
/bind ^+6 /echo Ctrl-Shift-6: not bound.
/bind ^+7 /echo Ctrl-Shift-7: not bound.
/bind ^+8 /echo Ctrl-Shift-8: not bound.
/bind ^+9 /echo Ctrl-Shift-9: not bound.
/bind ^+0 /echo Ctrl-Shift-0: not bound.
